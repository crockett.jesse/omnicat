
import React from 'react'
import { hot } from 'react-hot-loader'

const Biochemistry = () => (
  <div className="pure-u">
    <h2>Page for Biochemistry</h2>
    <hr/>
  </div>
)

export default hot(module)(Biochemistry)
