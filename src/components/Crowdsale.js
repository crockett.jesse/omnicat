import React from 'react'
import { hot } from 'react-hot-loader'


const Crowdsale = () => (
  <div className="pure-u">
    <h2>Page for the Crowdsale</h2>
    <hr/>
  </div>
)

export default hot(module)(Crowdsale)
