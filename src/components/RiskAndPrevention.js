
import React from 'react'
import { hot } from 'react-hot-loader'


const RiskAndPrevention = () => (
  <div className="pure-u">
    <h2>Page for Risk and Prevention</h2>
    <hr/>
  </div>
)

export default hot(module)(RiskAndPrevention)
