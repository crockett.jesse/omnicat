import React from 'react'
import { hot } from 'react-hot-loader'


const BrainHealth = () => (
  <div className="pure-u">
    <h2>Page for Brain Health</h2>
    <hr/>
  </div>
)

export default hot(module)(BrainHealth)
